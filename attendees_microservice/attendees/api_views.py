from django.http import JsonResponse
from .models import Attendee, ConferenceVO
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json


class AttendeeListEncoder(ModelEncoder):
    model=Attendee
    properties=[
        'name',
    ]

class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


@require_http_methods(["GET","POST"])
def api_list_attendees(request, conference_vo_id):
    """
    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )
    else:

        content = json.loads(request.body)

        try:
            conference_href = f'/api/conferences/{conference_vo_id}/'

            conference = ConferenceVO.objects.get(import_href=conference_href)

            content['conference'] = conference

        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {'message': "Invalid conference id"},
                status=400,
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False
        )


class AttendeeDetailEncoder(ModelEncoder):
    model=Attendee
    properties=[
        'email',
        'name',
        'company_name',
        'created',
        'conference',
    ]
    encoders = {
        'conference': ConferenceVODetailEncoder(),
    }



@require_http_methods(["GET","PUT","DELETE"])
def api_show_attendee(request, id):
    """
    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False
            )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        #PUT request
        content = json.loads(request.body)

            # check for conference
        if "conference" in content:
            try:
                # this might be wrong
                '''
                  if a conference id number is provided in the json body for editing an attendee
                  that id number should corespond to the unique conference_id number inside of monolith
                  that unique_id number is not gauranteed to correspond to the id number of the conferenceVO.
                    so the correct way of assigning an attendee to a new conference would be:

                        take the unique id number passed in with th json body,
                        query the monolith to retrieve the href for that specific conference,
                        .get the conferenceVO that has an import_href that corresponds to the href retrieved from the conference object
                        a better way would be to just pass the unique href as the value from the form so that it will correspond to the correct conference


                        ????
                '''



                conference = ConferenceVO.objects.get(id=content['conference'])
                content["conference"] = conference
            except ConferenceVO.DoesNotExist:
                JsonResponse(
                {'message': "Invalid conference id"},
                status=400,
                )

        Attendee.objects.filter(id=id).update(**content)

        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False
        )
