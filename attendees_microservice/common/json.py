from json import JSONEncoder
from django.db.models import QuerySet
from datetime import datetime


class DateEncoder(JSONEncoder):
    def default(self, o):
        # print('\n\n\n')
        # print("DateEncoder called")
        if isinstance(o, datetime):
            # print('current object is DateTime, here is the time formatted:')
            print(o.isoformat())
            return o.isoformat()
        else:
            # print('current item is not a DateTime Object, call parent')
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        # print('\n\n\n')
        # print("QuerysetEncoder called")
        if isinstance(o, QuerySet):
            # print('current object is Queryset, here is the item formatted:')
            # print(list(o))
            return list(o)
        else:
            # print('current item is not a Queryset Object, call parent')
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        # print('\n\n\n')
        # print(f"{self.model.__name__} ModelEncoder called")
        if isinstance(o, self.model):
            d = {}
            # print("-------->current dictionary:  ", d,'\n\n')
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
                # print("-------->current dictionary:  ", d,'\n\n')
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    # print('current item is in encoders, call the encoder')
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
                # print("-------->current dictionary:  ", d,'\n\n')
            d.update(self.get_extra_data(o))




            # print("here is the final version of our object:",d,"\n\n\n")
            return d


        else:
            # print(f'current item is not an instance of {self.model.__name__}, call parent')
            return super().default(o)

    def get_extra_data(self, o):
        return {}
