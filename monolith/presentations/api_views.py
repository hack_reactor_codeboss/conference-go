from django.http import JsonResponse
from .models import Presentation
from events.models import Conference
from common.json import ModelEncoder
from events.api_views import ConferenceListEncoder
from django.views.decorators.http import require_http_methods
import json

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        'title',
    ]

    def get_extra_data(self, o):
        return {"status": o.status.name}

@require_http_methods(["GET","POST"])
def api_list_presentations(request, conference_id):
    """
    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """

    if request.method == "GET":
        return JsonResponse(
            {"presentations": Presentation.objects.filter(conference=conference_id)},
            encoder=PresentationListEncoder
            )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {'message': "Invalid Conference id"},
                status= 400
            )
        presentation = Presentation.create(**content)

        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False
        )


class PresentationDetailEncoder(ModelEncoder):
    model=Presentation
    properties = [
        'presenter_name',
        'company_name',
        'presenter_email',
        'title',
        'synopsis',
        'created',
        'conference',
    ]

    encoders = {
        'conference':ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}

@require_http_methods(["GET","PUT","DELETE"])
def api_show_presentation(request, id):
    """
    {
        "presenter_name": the name of the presenter,
        "company_name": the name of the presenter's company,
        "presenter_email": the email address of the presenter,
        "title": the title of the presentation,
        "synopsis": the synopsis for the presentation,
        "created": the date/time when the record was created,
        "status": the name of the status for the presentation,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """

    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)

        # to change which conference the presentation is associated with
        # we need a way of grabbing the conference object befre updating
        # or do we? seems like we should not do this


        Presentation.objects.filter(id=id).update(**content)

        presentation = Presentation.objects.get(id=id)

        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False
        )
